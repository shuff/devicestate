﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeviceState
{
    public class CommandLineParser
    {
        Dictionary<string, Type> _namedParameters;
        List<KeyValuePair<string, Type>> _positionalParameters = new List<KeyValuePair<string, Type>>();

        public CommandLineParser()
        {
            _namedParameters = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase);
        }

        public CommandLineParser(CultureInfo culture, bool ignoreCase)
        {
            _namedParameters = new Dictionary<string, Type>(StringComparer.Create(culture, ignoreCase));
        }

        public void AddNamedParameter(string name, Type type)
        {
            _namedParameters.Add(name, type);
        }

        public void AddPositionalParameter(string name, Type type)
        {
            if (_positionalParameters.Count > 0) {
                if (_positionalParameters[_positionalParameters.Count - 1].Value.IsArray)
                    throw new CommandLineException("Can only have a single array positional parameter and it must be the last one.");
            }
            _positionalParameters.Add(new KeyValuePair<string, Type>(name, type));
        }

        static Regex _namedParser = new Regex("^-([a-zA-Z0-9_]+):(.*)$", RegexOptions.Compiled);
        public CommandLineArgs ParseArgs(string[] args)
        {
            Dictionary<string, object> namedArgs = new Dictionary<string, object>(_namedParameters.Comparer);
            List<object> positionalArgs = new List<object>();

            foreach (var arg in args) {
                var match = _namedParser.Match(arg);
                if (match.Success) {
                    Type type;
                    var namedArg = match.Groups[1].Value;
                    if (!_namedParameters.TryGetValue(namedArg, out type))
                        throw new CommandLineException(string.Format("Unknown argument {0}.", namedArg));
                    var value = match.Groups[2].Value;
                    if (type.IsArray) {
                        var converted = ConvertArgument(value, type.GetElementType());
                        object obj;
                        List<object> values;
                        if (!namedArgs.TryGetValue(namedArg, out obj))
                            namedArgs[namedArg] = values = new List<object>();
                        else
                            values = (List<object>)obj;
                        values.Add(converted);
                    }
                    else {
                        if (namedArgs.ContainsKey(namedArg))
                            throw new CommandLineException(string.Format("Duplicate argmument {0}.", arg));
                        var converted = ConvertArgument(value, type);
                        namedArgs[namedArg] = converted;
                    }
                }
                else {
                    if (_positionalParameters.Count == 0 || (positionalArgs.Count == _positionalParameters.Count && !_positionalParameters[_positionalParameters.Count - 1].Value.IsArray))
                        throw new CommandLineException(string.Format("Too many arguments at '{0}'", arg));
                    var type = _positionalParameters[positionalArgs.Count - (positionalArgs.Count == _positionalParameters.Count ? 1 : 0)].Value;
                    if (type.IsArray) {
                        List<object> values;
                        if (positionalArgs.Count == _positionalParameters.Count)
                            values = (List<object>)positionalArgs[positionalArgs.Count - 1];
                        else {
                            values = new List<object>();
                            positionalArgs.Add(values);
                        }
                        var converted = ConvertArgument(arg, type.GetElementType());
                        values.Add(converted);
                    }
                    else {
                        var converted = ConvertArgument(arg, type);
                        positionalArgs.Add(converted);
                    }
                }
            }

            if (_positionalParameters.Count > positionalArgs.Count)
                throw new CommandLineException("Not enough positional arguments specified.");

            foreach (var kv in namedArgs.ToList()) {
                Type type = _namedParameters[kv.Key];
                if (type.IsArray) {
                    namedArgs[kv.Key] = MakeArray((List<object>)kv.Value, type);
                }
            }

            for (int i = 0, l = positionalArgs.Count; i < l; ++i) {
                Type type = _positionalParameters[i].Value;
                if (type.IsArray) {
                    positionalArgs[i] = MakeArray((List<object>)positionalArgs[i], type);
                }
            }

            return new CommandLineArgs { NamedArgs = namedArgs, PositionalArgs = positionalArgs };
        }

        Array MakeArray(List<object> list, Type type)
        {
            var ary = Array.CreateInstance(type.GetElementType(), list.Count);
            for (int i = 0, l = list.Count; i < l; ++i)
                ary.SetValue(list[i], i);
            return ary;
        }

        object ConvertArgument(string value, Type type)
        {
            var converter = TypeDescriptor.GetConverter(type);
            if (converter.CanConvertFrom(typeof(string))) {
                try {
                    var obj = converter.ConvertFromString(value);
                    return obj;
                }
                catch {
                    // failed convert
                }
            }

            if (Attribute.IsDefined(type, typeof(SerializableAttribute))) {
                MemoryStream stm = new MemoryStream(Encoding.UTF8.GetBytes(value));
                try {
                    BinaryFormatter fmt = new BinaryFormatter();
                    var obj = fmt.Deserialize(stm);
                    if (type.IsInstanceOfType(obj))
                        return obj;
                }
                catch {
                    // Failed doing a binary deserialize
                }
            }

            try {
                return Convert.ChangeType(value, type);
            }
            catch {
                // Failed plain convert
            }

            throw new CommandLineException(string.Format("Unable to convert type type {0}: {1}", type.Name, value));
        }

        public string GenerateUsage(string command)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine()
                .Append(command)
                .Append(" (")
                .Append(Assembly.GetEntryAssembly().GetName().Version)
                .Append(")")
                .AppendLine();

            sb.AppendLine("Usage:").Append(command);

            if (_namedParameters.Count > 0)
                sb.Append(" [options...]");

            foreach (var position in _positionalParameters) {
                sb.Append(' ').Append(position.Key);
            }

            sb.AppendLine();

            if (_namedParameters.Count > 0) {
                sb.AppendLine("Options:");
                foreach (var kv in _namedParameters) {
                    sb.Append("    -")
                        .Append(kv.Key)
                        .Append(':')
                        .Append(kv.Value.Name)
                        .AppendLine();
                }
            }

            return sb.ToString();
        }
    }

    public class CommandLineArgs
    {
        public IDictionary<string, object> NamedArgs;
        public IList<object> PositionalArgs;

        public object GetNamedArg(string name)
        {
            object obj;
            if (!NamedArgs.TryGetValue(name, out obj))
                throw new CommandLineException(string.Format("Required argument '{0}' was missing.", name));
            return obj;
        }

        public T GetNamedArg<T>(string name)
        {
            return (T)GetNamedArg(name);
        }

        public object GetNamedArg(string name, object def)
        {
            object obj;
            if (!NamedArgs.TryGetValue(name, out obj))
                return def;
            return obj;
        }

        public T GetNamedArg<T>(string name, T def)
        {
            return (T)GetNamedArg(name, (object)def);
        }

        public object GetPositionalArg(int position)
        {
            if (position >= PositionalArgs.Count)
                throw new CommandLineException(string.Format("Required positional argument for postion {0} was missing.", position));
            return PositionalArgs[position];
        }

        public T GetPositionalArg<T>(int position)
        {
            return (T)GetPositionalArg(position);
        }

        public object GetPositionalArg(int position, object def)
        {
            if (position >= PositionalArgs.Count)
                return def;
            return PositionalArgs[position];
        }

        public T GetPositionalArg<T>(int position, T def)
        {
            return (T)GetPositionalArg(position, (object)def);
        }
    }

    [Serializable]
    public class CommandLineException : Exception
    {
        public CommandLineException() { }
        public CommandLineException(string message) : base(message) { }
        public CommandLineException(string message, Exception inner) : base(message, inner) { }
        protected CommandLineException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
