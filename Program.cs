﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceState
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                var options = ParseCommandLine(args);
                if (options != null)
                {
                    switch (options.Command)
                    {
                        case Command.Reset:
                            DeviceHelper.SetDeviceEnabled(options.DeviceClass, options.InstancePath, false);
                            DeviceHelper.SetDeviceEnabled(options.DeviceClass, options.InstancePath, true);
                            break;
                        case Command.Disable:
                            DeviceHelper.SetDeviceEnabled(options.DeviceClass, options.InstancePath, false);
                            break;
                        case Command.Enable:
                            DeviceHelper.SetDeviceEnabled(options.DeviceClass, options.InstancePath, true);
                            break;
                        default:
                            Usage();
                            break;
                    }
                }
                return 0;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                return 1;
            }
        }

        enum Command
        {
            None, Enable, Disable, Reset
        }

        class Options
        {
            public Guid DeviceClass;
            public string InstancePath;
            public Command Command;
        }

        static Options ParseCommandLine(string[] cl)
        {
            if (cl != null && cl.Length != 0)
            {

                var p = new CommandLineParser();
                p.AddPositionalParameter("class", typeof(string));
                p.AddPositionalParameter("instance", typeof(string));
                p.AddPositionalParameter("command", typeof(Command));

                var args = p.ParseArgs(cl);

                try
                {
                    return new Options
                    {
                        DeviceClass = Guid.Parse(args.GetPositionalArg<string>(0)),
                        InstancePath = args.GetPositionalArg<string>(1),
                        Command = args.GetPositionalArg<Command>(2, Command.None),
                    };
                }
                catch (CommandLineException exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
            Usage();
            return null;
        }

        static void Usage()
        {
            Console.WriteLine("{0} <deviceClassGuid> <deviceInstancePath> <enable|disable|reset>", GetExeName());
        }

        static string GetExeName()
        {
            return Path.GetFileName(new Uri(typeof(Program).Assembly.CodeBase).LocalPath);
        }

        static void EnableNIC(bool enable)
        {
            // every type of device has a hard-coded GUID, this is the one for mice
            Guid mouseGuid = new Guid("{4d36e972-e325-11ce-bfc1-08002be10318}");

            // get this from the properties dialog box of this device in Device Manager
            string instancePath = @"PCI\VEN_8086&DEV_08B1&SUBSYS_C4708086&REV_6B\28B2BDFFFF136EE200";

            DeviceHelper.SetDeviceEnabled(mouseGuid, instancePath, enable);
        }
    }
}
