Device State
============

This is a small app for enabling, disabling and cycling the enabled state of a
device in Windows.  It was written because all of my recent laptops with Intel
Dual band wireless AC network adapters don't recover wireless networking after
sleep or hibernation.  Disabling the device followed by re-enabling after awaking
from hibernation/sleep solves this problem for me.  

On my machines, the app is executed by a Windows scheduled task which
triggered when an event in the System log from source Power-Troubleshooter
having EventID = 1.

The arguments passed include the class GUID, the device instance path and
the verb, enable, disable or reset.  The class GUID and device instance path can
both be found in the driver properties dialog on the details tab.

Update: Powershell has commands to do this work already so a special app is not required.